(function() {
    var snake = function(n) {
        var matrix, i, j;

        var arr = function() {
            matrix = [];

            for (i = 0; i < n; i++) {
                matrix[i] = [];
            }
        };

        var fill = function() {
            var number = 0;

            i = -1;
            j = n - 1;
            while (number < n * n) {
                while (++i < n && !matrix[i][j])
                    matrix[i][j] = ++number;
                i--;
                while (--j >= 0 && !matrix[i][j])
                    matrix[i][j] = ++number;
                j++;
                while (--i >= 0 && !matrix[i][j])
                    matrix[i][j] = ++number;
                i++;
                while (++j < n && !matrix[i][j])
                    matrix[i][j] = ++number;
                j--;
            }
        };

        return function() {
            arr();
            fill();
            for (i = 0; i < n; i++) {
                for (j = 0; j < n; j++) {
                    console.log(matrix[i][j]);
                }
            }
        }();
    };

    snake(4);
})();
