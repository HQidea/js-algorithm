(function() {
    var sort = function(data) {
        var simple = function() {
            var min, i, j, temp;
            for (i = 0; i < data.length - 1; i++) {
                min = i;
                for (j = i + 1; j < data.length; j++) {
                    if (data[j] < data[min])
                        min = j;
                }
                temp = data[i];
                data[i] = data[min];
                data[min] = temp;
            }
        };
        simple();
        console.log(data);
    };
    sort([34, 28, 28, 79, 22, 16, 55]);
})();