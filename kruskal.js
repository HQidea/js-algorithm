(function() {
    "use strict";

    var kruskal = function() {
        var numVertex = 7, matrix = [];
        matrix[1] = [];
        matrix[2] = [];
        matrix[3] = [];
        matrix[4] = [];
        matrix[5] = [];
        matrix[6] = [];

        matrix[1][1] = 0;
        matrix[1][2] = 1;
        matrix[1][3] = 1;
        matrix[1][4] = 2;
        matrix[1][5] = 99;
        matrix[1][6] = 99;

        matrix[2][1] = 1;
        matrix[2][2] = 0;
        matrix[2][3] = 2;
        matrix[2][4] = 4;
        matrix[2][5] = 99;
        matrix[2][6] = 2;

        matrix[3][1] = 3;
        matrix[3][2] = 3;
        matrix[3][3] = 0;
        matrix[3][4] = 99;
        matrix[3][6] = 99;
        matrix[3][5] = 1;

        matrix[4][1] = 2;
        matrix[4][2] = 4;
        matrix[4][3] = 99;
        matrix[4][4] = 0;
        matrix[4][5] = 3;
        matrix[4][6] = 99;

        matrix[5][1] = 99;
        matrix[5][2] = 99;
        matrix[5][3] = 1;
        matrix[5][4] = 3;
        matrix[5][5] = 0;
        matrix[5][6] = 99;


        matrix[6][1] = 99;
        matrix[6][2] = 2;
        matrix[6][3] = 99;
        matrix[6][4] = 99;
        matrix[6][5] = 99;
        matrix[6][6] = 0;

        var minTree = function() {
            var i, j, used = [], sort = [], parent = [];

            for (i = 1; i < numVertex; i++) {
                parent[i] = 0;
                for (j = 1; j < numVertex; j++) {
                    sort.push({
                        weight: matrix[i][j],
                        i: i,
                        j: j
                    });
                }
            }
            sort.sort(function(a, b) {
                return a.weight - b.weight;
            });

            for (i = 0; i < sort.length; i++) {
                if (sort[i].weight == 0) {
                    continue;
                }
                var a = find(parent, sort[i].i);
                var b = find(parent, sort[i].j);

                if (a != b)
                {
                    parent[a] = b;
                    used.push(sort[i].i);
                    used.push(sort[i].j);
                }
                console.log(used);
            }
//console.log(sort);
        };

        /**
         * 关键所在，判断回路！
         * @param parent
         * @param i
         * @returns {*}
         */
        var find = function(parent, i) {
            while (parent[i] > 0) {
                i = parent[i];
            }
            return i;
        };
        minTree();
    };
    kruskal();
})();