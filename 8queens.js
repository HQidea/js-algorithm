(function() {
    var nQueens = function(n) {
        var count = 0, queen = [], exiCol = [], exiMDia = [], exiVDig = [];

        var search1 = function(row) {
            var i ,j;

            if (row === n) {
                // solved
                count++;
            }
            else {
                for (i = 0; i < n; i++) {
                    for (j = 0; j < row; j++) {
                        if (queen[j] === i || j + queen[j] === row + i || j - queen[j] === row - i) {
                            // dead end
                            break;
                        }
                    }
                    if (j === row) {
                        queen[row] = i;
                        search1(row + 1);
                    }
                }
            }
        };

        var search2 = function(row) {
            var i;

            if (row === n) {
                // solved
                count++;
            }
            else {
                for (i = 0; i < n; i++) {
                    if (!exiCol[i] && !exiMDia[i - row + n] && !exiVDig[i + row]) {
                        exiCol[i] = exiMDia[i - row + n] = exiVDig[i + row] = true;
                        search2(row + 1);
                        exiCol[i] = exiMDia[i - row + n] = exiVDig[i + row] = false;
                    }
                }
            }
        };

        // search1(0);
        search2(0);
        console.log(count);
    };
    nQueens(14);
})();