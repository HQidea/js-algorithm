(function() {
    "use strict";

    /**
     * 维护一个以起点（1）的lowCost，取其中最小值，然后加入某点后更新lowCost
     */
    var prim = function() {
        var numVertex = 7, matrix = [];
        matrix[1] = [];
        matrix[2] = [];
        matrix[3] = [];
        matrix[4] = [];
        matrix[5] = [];
        matrix[6] = [];

        matrix[1][1] = 0;
        matrix[1][2] = 1;
        matrix[1][3] = 3;
        matrix[1][4] = 2;
        matrix[1][5] = 99;
        matrix[1][6] = 99;

        matrix[2][1] = 1;
        matrix[2][2] = 0;
        matrix[2][3] = 3;
        matrix[2][4] = 4;
        matrix[2][5] = 99;
        matrix[2][6] = 2;

        matrix[3][1] = 3;
        matrix[3][2] = 3;
        matrix[3][3] = 0;
        matrix[3][4] = 99;
        matrix[3][6] = 99;
        matrix[3][5] = 1;

        matrix[4][1] = 2;
        matrix[4][2] = 4;
        matrix[4][2] = 99;
        matrix[4][4] = 0;
        matrix[4][5] = 3;
        matrix[4][5] = 99;

        matrix[5][1] = 99;
        matrix[5][2] = 99;
        matrix[5][3] = 1;
        matrix[5][4] = 3;
        matrix[5][5] = 99;
        matrix[5][6] = 99;


        matrix[6][1] = 99;
        matrix[6][2] = 2;
        matrix[6][3] = 99;
        matrix[6][4] = 99;
        matrix[6][5] = 99;
        matrix[6][6] = 99;


        var minTree = function() {
            var i, j, lowCost = [], vertex;

            for (i = 2; i < numVertex; i++) {
                lowCost[i] = matrix[1][i];
            }

            for (i = 2; i < numVertex; i++) {
                // get minimal lowCost
                vertex = lowest(lowCost);
                console.log(vertex, lowCost[vertex]);
                lowCost[vertex] = 0;


                for (j = 2; j < numVertex; j++) {
                    if (j === vertex) {
                        continue;
                    }
//console.log(vertex);
                    if (matrix[vertex][j] < lowCost[j]) {
                        lowCost[j] = matrix[vertex][j];
                    }
                }

            }
            console.log(lowCost);
        };

        var lowest = function(lowCost) {
            var min = Infinity, j;
            lowCost.forEach(function(c, i) {
                if (c > 0 && c < min) {
                    min = c;
                    j = i;
                }
            });
            return j;
        };

        minTree();
    };
    prim();
})();