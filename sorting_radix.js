(function() {
    var sort = function(data) {
        var i, j, k, factor, radix = 10, digits = 10, queue = [];
        for (i = 0; i < radix; i++) {
            queue[i] = [];
        }
        console.log(queue);
        for (i = 0, factor = 1; i < digits; i++, factor *= radix) {
            for (j = 0; j < data.length; j++) {
                queue[Math.floor(data[j] / factor) % radix].push(data[j]);
            }
            for (k = j = 0; j < radix; j++) {
                while (queue[j].length) {
                    data[k++] = queue[j].shift();
                }
            }
        }
        console.log(data);
    };
    sort([24, 2, 56, 102, 98, 4, 81, 112]);
})();