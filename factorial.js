(function() {
    var factorial = function(n) {
        var result = [];

        var calculator = function() {
            var i, j, temp, carry;

            result[0] = 1;

            if (n < 2)
                return;

            for (i = 2; i <= n; i++) {
                carry = 0;
                for (j = 0; j < result.length; j++) {
                    temp = result[j] * i + carry;
                    carry = Math.floor(temp / 10);
                    result[j] = temp % 10;

                    if (j === result.length - 1 && carry) {
                        result[j + 1] = 0;
                    }
                }
            }
        };

        return function() {
            calculator();
            console.log(result.reverse().join(''));
        }();
    };

    factorial(20);
})();