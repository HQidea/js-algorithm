(function() {
    "use strict";
    var sort = function(data) {
        debugger;
        var quickSort = function(low, high) {
            if (low >= high)
                return;

            var pivot = partition(low, high);
            quickSort(low, pivot - 1);
            quickSort(pivot + 1, high);
        };
        var partition = function(low, high) {
            var pivot = data[low];
            while (low < high) {
                while (low < high && data[high] >= pivot) {
                    high--;
                }
                data[low] = data[high];

                while (low < high && pivot >= data[low]) {
                    low++;
                }
                data[high] = data[low];
            }
            data[low] = pivot;
            return low;
        };
        var quickSort2 = function(begin, end) {
            if (begin >= end)
                return;
            var pivot = data[begin], small, big;
            var i = begin, j = end;
            while (i < j) {
                for (; i < j; i++) {
                    if (data[i] > pivot) {
                        big = data[i];
                        break;
                    }
                }
                for (; j > i; j--) {
                    if (data[j] < pivot) {
                        small = data[j];
                        data[j] = big;
                        data[i] = small;
                        break;
                    }
                }
            }
            while (data[j] > pivot) {
                j--;
            }
            small = data[j];
            data[j] = pivot;
            data[begin] = small;
            quickSort2(begin, i - 1);
            quickSort2(i + 1, end);
        };
        var quickSort3 = function(start, end) {
            if (start >= end)
                return;
            var pivot = data[start],
                i = start,
                j = end;
            while (i < j) {
                while (i < j && data[j] >= pivot) {
                    j--;
                }
                data[i] = data[j];

                while (i < j && data[i] <= pivot) {
                    i++;
                }
                data[j] = data[i];

            }
            data[i] = pivot;
            quickSort3(start, i - 1);
            quickSort3(i + 1, end);
        };
        var quickSort4 = function() {
            function quickSort(array, left, right) {
                if (Object.prototype.toString.call(array).slice(8, -1) === 'Array' && typeof left === 'number' && typeof right === 'number') {
                    if (left < right) {
                        var x = array[right], i = left - 1, temp;
                        for (var j = left; j <= right; j++) {
                            if (array[j] <= x) {
                                i++;
                                temp = array[i];
                                array[i] = array[j];
                                array[j] = temp;
                            }
                        }
                        quickSort(array, left, i - 1);
                        quickSort(array, i + 1, right);
                    };
                } else {
                    return 'array is not an Array or left or right is not a number!';
                }
            }
            var aaa = [3, 5, 2, 9, 1, 3, 6, 7, 10];
            quickSort(aaa, 0, aaa.length - 1);
            console.log(aaa);
        };
        quickSort4(0, data.length - 1);
        console.log(data);
    };
    sort([34, 28, 28, 79, 22, 16, 55]);
})();