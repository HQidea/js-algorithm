(function() {
    var sort = function(data) {
        var heap = function(n) {
            // 建堆
            for (var i = Math.floor(n / 2) - 1; i >= 0; i--) {
                heapAdjust(i, n);
            }
            console.log(data);
            // 调整堆
            for (i = n - 1; i > 0; i--) {
                var tmp = data[0];
                data[0] = data[i];
                data[i] = tmp;
                heapAdjust(0, i);
            }
        };
        var heapAdjust = function(i, n) { // n 为length
            var child;
            for (var tmp = data[i]; 2 * i + 1 < n; i = child) {
                child = 2 * i + 1;
                if (child != n - 1 && data[child + 1] > data[child]) {
                    child++;
                }
                if (tmp < data[child]) {
                    data[i] = data[child];
                }
                else
                    break;
            }
            data[i] = tmp;
        };
        var heap2 = function() {
            /*方法说明：堆排序
             @param  array 待排序数组*/
            function heapSort(array) {
                if (Object.prototype.toString.call(array).slice(8, -1) === 'Array') {
                    //建堆
                    var heapSize = array.length, temp;
                    for (var i = Math.floor(heapSize / 2); i >= 0; i--) {
                        heapify(array, i, heapSize);
                    }

                    //堆排序
                    for (var j = heapSize - 1; j >= 1; j--) {
                        temp = array[0];
                        array[0] = array[j];
                        array[j] = temp;
                        heapify(array, 0, --heapSize);
                    }
                    console.log(array);
                } else {
                    return 'array is not an Array!';
                }
            }
            /*方法说明：维护堆的性质
             @param  arr 数组
             @param  x   数组下标
             @param  len 堆大小*/
            function heapify(arr, x, len) {
                if (Object.prototype.toString.call(arr).slice(8, -1) === 'Array' && typeof x === 'number') {
                    var l = 2 * x, r = 2 * x + 1, largest = x, temp;
                    if (l < len && arr[l] > arr[largest]) {
                        largest = l;
                    }
                    if (r < len && arr[r] > arr[largest]) {
                        largest = r;
                    }
                    if (largest != x) {
                        temp = arr[x];
                        arr[x] = arr[largest];
                        arr[largest] = temp;
                        heapify(arr, largest, len);
                    }
                } else {
                    return 'arr is not an Array or x is not a number!';
                }
            }
            heapSort([98, 53, 55, 18, 4, 22, 24]);
        };
        heap(data.length);
        console.log(data);
    };
    sort([ 55, 18, 98,22, 24, 4,53]);
})();