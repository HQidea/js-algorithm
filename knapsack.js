(function() {
  "use strict";
  var knapsack = function() {
    var totalWeight = 10;
    var numItem = 4;
    var valueItems = [10, 40, 30, 50];
    var weightItems = [5, 4, 6, 3];

    var solve = function() {
      var i, j, result = [];
      for (i = 0; i < numItem; i++) {
        result[i] = [];
      }

      for (i = 0; i < numItem; i++) {
        for (j = 0; j < totalWeight; j++) {
          if (i === 0) {
            if (j < weightItems[i]) {
              result[i][j] = 0;
            }
            else {
              result[i][j] = valueItems[i];
            }
          }
          else {
            if (j < weightItems[i]) {
              result[i][j] = result[i - 1][j];
            }
            else {
              result[i][j] = Math.max(valueItems[i] + result[i - 1][j - weightItems[i]], result[i - 1][j]);
            }
          }
        }
      }
      console.log(result);
    };
    var solve2 = function() {
      var i, j, result = [];

      for (i = 0; i < numItem; i++) {
        for (j = totalWeight - 1; j >= 0; j--) {
          if (i === 0) {
            if (j < weightItems[i]) {
              result[j] = 0;
            }
            else {
              result[j] = valueItems[i];
            }
          }
          else {
            if (j >= weightItems[i]) {
              result[j] = Math.max(valueItems[i] + result[j - weightItems[i]], result[j]);
            }
          }
        }
      }
      console.log(result);
    };
    solve2();
  };
  knapsack();
})();