(function() {
    var sosf = function(n) {

        var getNext = function(num) {
            var i, j, str, temp;

            str = String.prototype.split.call(num, '');
            for (i = 0; i < str.length - 1; i++) {  //better
                for (j = i + 1; j < str.length; j++) {
                    if (str[i] > str[j]) {
                        temp = str[i];
                        str[i] = str[j];
                        str[j] = temp;
                    }
                }
            }
            /*var last = str.length - 1;
            for (i = 0; i < str.length; i++) {
                for (j = 0; j < last; j++) {
                    if (str[j] > str[j + 1]) {
                        temp = str[j];
                        str[j] = str[j + 1];
                        str[j + 1] = temp;
                    }
                }
                last--;
            }*/

            return str.reverse().join('') - str.reverse().join('');
        };

        var loop = function() {
            var i, found = 0, next = n, th = [next];

            while (true) {
                next = getNext(next);
                for (i = 0; i < th.length; i++) {
                    if (next === th[i]) {
                        found = 1;
                        break;
                    }
                }

                if (!found)
                    th.push(next);
                else
                    break;
            };

            return th;
        };

        return function() {
            console.log(loop());
        }();
    };

    var start = +new Date();
    sosf(1234);
    console.log(+new Date() - start);
})();