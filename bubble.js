(function() {
    "use strict";
    var sort = function(data) {
        var bubble = function() {
            var temp, j = 0;
            while (j++ < data.length - 1)
                for (var i = 0; i < data.length - j; i++) {
                    if (data[i] > data[i + 1]) {
                        temp = data[i];
                        data[i] = data[i + 1];
                        data[i + 1] = temp;
                    }
                }
        };
        var bubble2 = function() {
            var temp, j = 0, isBubbled = true;
            while (isBubbled) {
                j++;
                isBubbled = false;
                for (var i = 0; i < data.length - j; i++) {
                    if (data[i] > data[i + 1]) {
                        temp = data[i];
                        data[i] = data[i + 1];
                        data[i + 1] = temp;
                        isBubbled = true;
                    }
                }
            }
        };
        bubble2();
        console.log(data);
    };
    sort([34, 28, 28, 79, 22, 16, 55]);
})();