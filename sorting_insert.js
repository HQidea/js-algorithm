(function() {
    "use strict";
    var sort = function(data) {
        var insert0 = function() {
            var temp;
            for (var i = 1; i < data.length; i++) {
                while (i && data[i] < data[i - 1]) {
                    temp = data[i];
                    data[i] = data[i - 1];
                    data[i - 1] = temp;
                    i--;
                }
            }
        };
        var insert = function() {
            var temp;
            for (var i = 1; i < data.length; i++) {
                temp = data[i];
                while (i && temp < data[i - 1]) {
                    //temp = data[i];
                    data[i] = data[i - 1];
                    //data[i - 1] = temp;
                    i--;
                }
                data[i] = temp;
            }
        };
        var insertHalf = function() {
            var temp, low, mid, high;
            for (var i = 1; i < data.length; i++) {
                temp = data[i];
                low = 0;
                high = i - 1;

                while (low <= high) {
                    mid = Math.floor((low + high) / 2);
                    //debugger;
                    if (temp < data[mid]) {
                        high = mid - 1;
                    }
                    else {
                        low = mid + 1;
                    }
                }

                for (var j = i; j > low; j--) {
                    data[j] = data[j - 1];
                }
                data[low] = temp;
            }
        };
        var insertShell = function() {
            var increments = [5, 3, 1];
            for (var j = 0; j < increments.length; j++) {
                var temp;
                for (var i = increments[j]; i < data.length; i++) {
                    temp = data[i];
                    while (i >= increments[j] && temp < data[i - increments[j]]) {
                        //temp = data[i];
                        data[i] = data[i - increments[j]];
                        //data[i - 1] = temp;
                        i -= increments[j];
                    }
                    data[i] = temp;
                }
            }
        };
        insertShell();
        console.log(data);
    };
    sort([34, 28, 28, 79, 22, 16, 55]);
})();