(function() {
    "use strict";

    var dijkstra = function() {
        var numVertex = 8, matrix = [];
        matrix[1] = [];
        matrix[2] = [];
        matrix[3] = [];
        matrix[4] = [];
        matrix[5] = [];
        matrix[6] = [];
        matrix[7] = [];

        matrix[1][2] = 10;
        matrix[1][3] = 30;
        matrix[1][4] = 100;
        matrix[2][3] = 20;
        matrix[2][5] = 20;
        matrix[3][5] = 20;
        matrix[3][6] = 40;
        matrix[5][6] = 10;
        matrix[6][4] = 20;
        matrix[7][5] = 5;
        matrix[7][6] = 25;

        var minDist = function() {
            var i, j, table = [];

            for (i = 1; i < numVertex; i++) {
                table[i] = {
                    known: false,
                    dist: Infinity,
                    perVer: -1
                };
            }
            table[1].dist = 0;

            for (i = 1; i < numVertex; i++) {
                // minimum dist
                var v = minTab(table);
                // console.log(v);
                if (!v)
                    break;
                table[v].known = true;
                for (j = 2; j < numVertex; j++) {
                    if (!table[j].known && matrix[v][j] && table[v].dist + matrix[v][j] < table[j].dist) {
                        table[j].dist = table[v].dist + matrix[v][j];
                        table[j].perVer = v;
                    }
                }
            }
            console.log(table);
        };

        var minTab = function(table) {
            var min = Infinity, index;
            table.forEach(function(t, i) {
                if (!t.known && t.dist < min) {
                    min = t.dist;
                    index = i;
                }
            });
            return index;
        };

        minDist();
    };
    dijkstra();
})();